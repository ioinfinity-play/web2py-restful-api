FROM python:3.6

#LABEL your_label

RUN apt-get update && \
 apt-get install -y python python-pip python-setuptools unzip wget python-gunicorn git&& \
 pip install virtualenv 

RUN groupadd -r web2py && \
 useradd -m -r -g web2py web2py

USER web2py

RUN virtualenv /home/web2py && \
 rm -rf /home/web2py/web2py && \
 cd /home/web2py/ && \
 git clone --recursive https://github.com/web2py/web2py.git && \
 chmod 777 -R /home/web2py/web2py

WORKDIR /home/web2py/web2py

COPY parameters_8080.py parameters_8080.py

EXPOSE 8080

CMD . /home/web2py/bin/activate && /usr/bin/python /home/web2py/web2py/anyserver.py -s gunicorn -i 0.0.0.0 -p 8080
